/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.session;

import alfamindtransactionmigration.entity.Purchases;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import alfamindtransactionmigration.entity.ShopOwners;
import alfamindtransactionmigration.session.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

/**
 *
 * @author rudythong
 */
public class PurchasesJpaController implements Serializable {

    public PurchasesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Purchases purchases) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ShopOwners shopOwnerId = purchases.getShopOwnerId();
            if (shopOwnerId != null) {
                shopOwnerId = em.getReference(shopOwnerId.getClass(), shopOwnerId.getShopOwnerId());
                purchases.setShopOwnerId(shopOwnerId);
            }
            em.persist(purchases);
            if (shopOwnerId != null) {
                shopOwnerId.getPurchasesCollection().add(purchases);
                shopOwnerId = em.merge(shopOwnerId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Purchases purchases) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Purchases persistentPurchases = em.find(Purchases.class, purchases.getPurchaseId());
            ShopOwners shopOwnerIdOld = persistentPurchases.getShopOwnerId();
            ShopOwners shopOwnerIdNew = purchases.getShopOwnerId();
            if (shopOwnerIdNew != null) {
                shopOwnerIdNew = em.getReference(shopOwnerIdNew.getClass(), shopOwnerIdNew.getShopOwnerId());
                purchases.setShopOwnerId(shopOwnerIdNew);
            }
            purchases = em.merge(purchases);
            if (shopOwnerIdOld != null && !shopOwnerIdOld.equals(shopOwnerIdNew)) {
                shopOwnerIdOld.getPurchasesCollection().remove(purchases);
                shopOwnerIdOld = em.merge(shopOwnerIdOld);
            }
            if (shopOwnerIdNew != null && !shopOwnerIdNew.equals(shopOwnerIdOld)) {
                shopOwnerIdNew.getPurchasesCollection().add(purchases);
                shopOwnerIdNew = em.merge(shopOwnerIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = purchases.getPurchaseId();
                if (findPurchases(id) == null) {
                    throw new NonexistentEntityException("The purchases with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Purchases purchases;
            try {
                purchases = em.getReference(Purchases.class, id);
                purchases.getPurchaseId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The purchases with id " + id + " no longer exists.", enfe);
            }
            ShopOwners shopOwnerId = purchases.getShopOwnerId();
            if (shopOwnerId != null) {
                shopOwnerId.getPurchasesCollection().remove(purchases);
                shopOwnerId = em.merge(shopOwnerId);
            }
            em.remove(purchases);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Purchases> findPurchasesEntities() {
        return findPurchasesEntities(true, -1, -1);
    }

    public List<Purchases> findPurchasesEntities(int maxResults, int firstResult) {
        return findPurchasesEntities(false, maxResults, firstResult);
    }

    private List<Purchases> findPurchasesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Purchases.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Purchases findPurchases(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Purchases.class, id);
        } finally {
            em.close();
        }
    }

    public int getPurchasesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Purchases> rt = cq.from(Purchases.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public Purchases findByInvoiceNumber(String invoiceNumber) {
        try {
        return (Purchases) getEntityManager().createNamedQuery("Purchases.findByInvoiceNumber").setParameter("invoiceNumber", invoiceNumber).getSingleResult();
        } catch (NoResultException | NonUniqueResultException nre) {
            return null;
        }
    }
}
