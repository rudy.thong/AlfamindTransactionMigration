/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.session;

import alfamindtransactionmigration.entity.Merchants;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import alfamindtransactionmigration.entity.ShopOwners;
import alfamindtransactionmigration.session.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author rudythong
 */
public class MerchantsJpaController implements Serializable {

    public MerchantsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Merchants merchants) {
        if (merchants.getShopOwnersCollection() == null) {
            merchants.setShopOwnersCollection(new ArrayList<ShopOwners>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<ShopOwners> attachedShopOwnersCollection = new ArrayList<ShopOwners>();
            for (ShopOwners shopOwnersCollectionShopOwnersToAttach : merchants.getShopOwnersCollection()) {
                shopOwnersCollectionShopOwnersToAttach = em.getReference(shopOwnersCollectionShopOwnersToAttach.getClass(), shopOwnersCollectionShopOwnersToAttach.getShopOwnerId());
                attachedShopOwnersCollection.add(shopOwnersCollectionShopOwnersToAttach);
            }
            merchants.setShopOwnersCollection(attachedShopOwnersCollection);
            em.persist(merchants);
            for (ShopOwners shopOwnersCollectionShopOwners : merchants.getShopOwnersCollection()) {
                Merchants oldMerchantIdOfShopOwnersCollectionShopOwners = shopOwnersCollectionShopOwners.getMerchantId();
                shopOwnersCollectionShopOwners.setMerchantId(merchants);
                shopOwnersCollectionShopOwners = em.merge(shopOwnersCollectionShopOwners);
                if (oldMerchantIdOfShopOwnersCollectionShopOwners != null) {
                    oldMerchantIdOfShopOwnersCollectionShopOwners.getShopOwnersCollection().remove(shopOwnersCollectionShopOwners);
                    oldMerchantIdOfShopOwnersCollectionShopOwners = em.merge(oldMerchantIdOfShopOwnersCollectionShopOwners);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Merchants merchants) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Merchants persistentMerchants = em.find(Merchants.class, merchants.getMerchantId());
            Collection<ShopOwners> shopOwnersCollectionOld = persistentMerchants.getShopOwnersCollection();
            Collection<ShopOwners> shopOwnersCollectionNew = merchants.getShopOwnersCollection();
            Collection<ShopOwners> attachedShopOwnersCollectionNew = new ArrayList<ShopOwners>();
            for (ShopOwners shopOwnersCollectionNewShopOwnersToAttach : shopOwnersCollectionNew) {
                shopOwnersCollectionNewShopOwnersToAttach = em.getReference(shopOwnersCollectionNewShopOwnersToAttach.getClass(), shopOwnersCollectionNewShopOwnersToAttach.getShopOwnerId());
                attachedShopOwnersCollectionNew.add(shopOwnersCollectionNewShopOwnersToAttach);
            }
            shopOwnersCollectionNew = attachedShopOwnersCollectionNew;
            merchants.setShopOwnersCollection(shopOwnersCollectionNew);
            merchants = em.merge(merchants);
            for (ShopOwners shopOwnersCollectionOldShopOwners : shopOwnersCollectionOld) {
                if (!shopOwnersCollectionNew.contains(shopOwnersCollectionOldShopOwners)) {
                    shopOwnersCollectionOldShopOwners.setMerchantId(null);
                    shopOwnersCollectionOldShopOwners = em.merge(shopOwnersCollectionOldShopOwners);
                }
            }
            for (ShopOwners shopOwnersCollectionNewShopOwners : shopOwnersCollectionNew) {
                if (!shopOwnersCollectionOld.contains(shopOwnersCollectionNewShopOwners)) {
                    Merchants oldMerchantIdOfShopOwnersCollectionNewShopOwners = shopOwnersCollectionNewShopOwners.getMerchantId();
                    shopOwnersCollectionNewShopOwners.setMerchantId(merchants);
                    shopOwnersCollectionNewShopOwners = em.merge(shopOwnersCollectionNewShopOwners);
                    if (oldMerchantIdOfShopOwnersCollectionNewShopOwners != null && !oldMerchantIdOfShopOwnersCollectionNewShopOwners.equals(merchants)) {
                        oldMerchantIdOfShopOwnersCollectionNewShopOwners.getShopOwnersCollection().remove(shopOwnersCollectionNewShopOwners);
                        oldMerchantIdOfShopOwnersCollectionNewShopOwners = em.merge(oldMerchantIdOfShopOwnersCollectionNewShopOwners);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = merchants.getMerchantId();
                if (findMerchants(id) == null) {
                    throw new NonexistentEntityException("The merchants with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Merchants merchants;
            try {
                merchants = em.getReference(Merchants.class, id);
                merchants.getMerchantId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The merchants with id " + id + " no longer exists.", enfe);
            }
            Collection<ShopOwners> shopOwnersCollection = merchants.getShopOwnersCollection();
            for (ShopOwners shopOwnersCollectionShopOwners : shopOwnersCollection) {
                shopOwnersCollectionShopOwners.setMerchantId(null);
                shopOwnersCollectionShopOwners = em.merge(shopOwnersCollectionShopOwners);
            }
            em.remove(merchants);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Merchants> findMerchantsEntities() {
        return findMerchantsEntities(true, -1, -1);
    }

    public List<Merchants> findMerchantsEntities(int maxResults, int firstResult) {
        return findMerchantsEntities(false, maxResults, firstResult);
    }

    private List<Merchants> findMerchantsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Merchants.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Merchants findMerchants(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Merchants.class, id);
        } finally {
            em.close();
        }
    }

    public int getMerchantsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Merchants> rt = cq.from(Merchants.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
