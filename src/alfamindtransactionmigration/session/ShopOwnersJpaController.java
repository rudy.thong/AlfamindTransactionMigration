/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.session;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import alfamindtransactionmigration.entity.Merchants;
import alfamindtransactionmigration.entity.Topups;
import java.util.ArrayList;
import java.util.Collection;
import alfamindtransactionmigration.entity.Purchases;
import alfamindtransactionmigration.entity.BalanceDeposit;
import alfamindtransactionmigration.entity.ShopOwners;
import alfamindtransactionmigration.session.exceptions.NonexistentEntityException;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

/**
 *
 * @author rudythong
 */
public class ShopOwnersJpaController implements Serializable {

    public ShopOwnersJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ShopOwners shopOwners) {
        if (shopOwners.getTopupsCollection() == null) {
            shopOwners.setTopupsCollection(new ArrayList<Topups>());
        }
        if (shopOwners.getPurchasesCollection() == null) {
            shopOwners.setPurchasesCollection(new ArrayList<Purchases>());
        }
        if (shopOwners.getBalanceDepositCollection() == null) {
            shopOwners.setBalanceDepositCollection(new ArrayList<BalanceDeposit>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Merchants merchantId = shopOwners.getMerchantId();
            if (merchantId != null) {
                merchantId = em.getReference(merchantId.getClass(), merchantId.getMerchantId());
                shopOwners.setMerchantId(merchantId);
            }
            Collection<Topups> attachedTopupsCollection = new ArrayList<Topups>();
            for (Topups topupsCollectionTopupsToAttach : shopOwners.getTopupsCollection()) {
                topupsCollectionTopupsToAttach = em.getReference(topupsCollectionTopupsToAttach.getClass(), topupsCollectionTopupsToAttach.getTopupId());
                attachedTopupsCollection.add(topupsCollectionTopupsToAttach);
            }
            shopOwners.setTopupsCollection(attachedTopupsCollection);
            Collection<Purchases> attachedPurchasesCollection = new ArrayList<Purchases>();
            for (Purchases purchasesCollectionPurchasesToAttach : shopOwners.getPurchasesCollection()) {
                purchasesCollectionPurchasesToAttach = em.getReference(purchasesCollectionPurchasesToAttach.getClass(), purchasesCollectionPurchasesToAttach.getPurchaseId());
                attachedPurchasesCollection.add(purchasesCollectionPurchasesToAttach);
            }
            shopOwners.setPurchasesCollection(attachedPurchasesCollection);
            Collection<BalanceDeposit> attachedBalanceDepositCollection = new ArrayList<BalanceDeposit>();
            for (BalanceDeposit balanceDepositCollectionBalanceDepositToAttach : shopOwners.getBalanceDepositCollection()) {
                balanceDepositCollectionBalanceDepositToAttach = em.getReference(balanceDepositCollectionBalanceDepositToAttach.getClass(), balanceDepositCollectionBalanceDepositToAttach.getBalanceDepositId());
                attachedBalanceDepositCollection.add(balanceDepositCollectionBalanceDepositToAttach);
            }
            shopOwners.setBalanceDepositCollection(attachedBalanceDepositCollection);
            em.persist(shopOwners);
            if (merchantId != null) {
                merchantId.getShopOwnersCollection().add(shopOwners);
                merchantId = em.merge(merchantId);
            }
            for (Topups topupsCollectionTopups : shopOwners.getTopupsCollection()) {
                ShopOwners oldShopOwnerIdOfTopupsCollectionTopups = topupsCollectionTopups.getShopOwnerId();
                topupsCollectionTopups.setShopOwnerId(shopOwners);
                topupsCollectionTopups = em.merge(topupsCollectionTopups);
                if (oldShopOwnerIdOfTopupsCollectionTopups != null) {
                    oldShopOwnerIdOfTopupsCollectionTopups.getTopupsCollection().remove(topupsCollectionTopups);
                    oldShopOwnerIdOfTopupsCollectionTopups = em.merge(oldShopOwnerIdOfTopupsCollectionTopups);
                }
            }
            for (Purchases purchasesCollectionPurchases : shopOwners.getPurchasesCollection()) {
                ShopOwners oldShopOwnerIdOfPurchasesCollectionPurchases = purchasesCollectionPurchases.getShopOwnerId();
                purchasesCollectionPurchases.setShopOwnerId(shopOwners);
                purchasesCollectionPurchases = em.merge(purchasesCollectionPurchases);
                if (oldShopOwnerIdOfPurchasesCollectionPurchases != null) {
                    oldShopOwnerIdOfPurchasesCollectionPurchases.getPurchasesCollection().remove(purchasesCollectionPurchases);
                    oldShopOwnerIdOfPurchasesCollectionPurchases = em.merge(oldShopOwnerIdOfPurchasesCollectionPurchases);
                }
            }
            for (BalanceDeposit balanceDepositCollectionBalanceDeposit : shopOwners.getBalanceDepositCollection()) {
                ShopOwners oldShopOwnerIdOfBalanceDepositCollectionBalanceDeposit = balanceDepositCollectionBalanceDeposit.getShopOwnerId();
                balanceDepositCollectionBalanceDeposit.setShopOwnerId(shopOwners);
                balanceDepositCollectionBalanceDeposit = em.merge(balanceDepositCollectionBalanceDeposit);
                if (oldShopOwnerIdOfBalanceDepositCollectionBalanceDeposit != null) {
                    oldShopOwnerIdOfBalanceDepositCollectionBalanceDeposit.getBalanceDepositCollection().remove(balanceDepositCollectionBalanceDeposit);
                    oldShopOwnerIdOfBalanceDepositCollectionBalanceDeposit = em.merge(oldShopOwnerIdOfBalanceDepositCollectionBalanceDeposit);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ShopOwners shopOwners) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ShopOwners persistentShopOwners = em.find(ShopOwners.class, shopOwners.getShopOwnerId());
            Merchants merchantIdOld = persistentShopOwners.getMerchantId();
            Merchants merchantIdNew = shopOwners.getMerchantId();
            Collection<Topups> topupsCollectionOld = persistentShopOwners.getTopupsCollection();
            Collection<Topups> topupsCollectionNew = shopOwners.getTopupsCollection();
            Collection<Purchases> purchasesCollectionOld = persistentShopOwners.getPurchasesCollection();
            Collection<Purchases> purchasesCollectionNew = shopOwners.getPurchasesCollection();
            Collection<BalanceDeposit> balanceDepositCollectionOld = persistentShopOwners.getBalanceDepositCollection();
            Collection<BalanceDeposit> balanceDepositCollectionNew = shopOwners.getBalanceDepositCollection();
            if (merchantIdNew != null) {
                merchantIdNew = em.getReference(merchantIdNew.getClass(), merchantIdNew.getMerchantId());
                shopOwners.setMerchantId(merchantIdNew);
            }
            Collection<Topups> attachedTopupsCollectionNew = new ArrayList<Topups>();
            for (Topups topupsCollectionNewTopupsToAttach : topupsCollectionNew) {
                topupsCollectionNewTopupsToAttach = em.getReference(topupsCollectionNewTopupsToAttach.getClass(), topupsCollectionNewTopupsToAttach.getTopupId());
                attachedTopupsCollectionNew.add(topupsCollectionNewTopupsToAttach);
            }
            topupsCollectionNew = attachedTopupsCollectionNew;
            shopOwners.setTopupsCollection(topupsCollectionNew);
            Collection<Purchases> attachedPurchasesCollectionNew = new ArrayList<Purchases>();
            for (Purchases purchasesCollectionNewPurchasesToAttach : purchasesCollectionNew) {
                purchasesCollectionNewPurchasesToAttach = em.getReference(purchasesCollectionNewPurchasesToAttach.getClass(), purchasesCollectionNewPurchasesToAttach.getPurchaseId());
                attachedPurchasesCollectionNew.add(purchasesCollectionNewPurchasesToAttach);
            }
            purchasesCollectionNew = attachedPurchasesCollectionNew;
            shopOwners.setPurchasesCollection(purchasesCollectionNew);
            Collection<BalanceDeposit> attachedBalanceDepositCollectionNew = new ArrayList<BalanceDeposit>();
            for (BalanceDeposit balanceDepositCollectionNewBalanceDepositToAttach : balanceDepositCollectionNew) {
                balanceDepositCollectionNewBalanceDepositToAttach = em.getReference(balanceDepositCollectionNewBalanceDepositToAttach.getClass(), balanceDepositCollectionNewBalanceDepositToAttach.getBalanceDepositId());
                attachedBalanceDepositCollectionNew.add(balanceDepositCollectionNewBalanceDepositToAttach);
            }
            balanceDepositCollectionNew = attachedBalanceDepositCollectionNew;
            shopOwners.setBalanceDepositCollection(balanceDepositCollectionNew);
            shopOwners = em.merge(shopOwners);
            if (merchantIdOld != null && !merchantIdOld.equals(merchantIdNew)) {
                merchantIdOld.getShopOwnersCollection().remove(shopOwners);
                merchantIdOld = em.merge(merchantIdOld);
            }
            if (merchantIdNew != null && !merchantIdNew.equals(merchantIdOld)) {
                merchantIdNew.getShopOwnersCollection().add(shopOwners);
                merchantIdNew = em.merge(merchantIdNew);
            }
            for (Topups topupsCollectionOldTopups : topupsCollectionOld) {
                if (!topupsCollectionNew.contains(topupsCollectionOldTopups)) {
                    topupsCollectionOldTopups.setShopOwnerId(null);
                    topupsCollectionOldTopups = em.merge(topupsCollectionOldTopups);
                }
            }
            for (Topups topupsCollectionNewTopups : topupsCollectionNew) {
                if (!topupsCollectionOld.contains(topupsCollectionNewTopups)) {
                    ShopOwners oldShopOwnerIdOfTopupsCollectionNewTopups = topupsCollectionNewTopups.getShopOwnerId();
                    topupsCollectionNewTopups.setShopOwnerId(shopOwners);
                    topupsCollectionNewTopups = em.merge(topupsCollectionNewTopups);
                    if (oldShopOwnerIdOfTopupsCollectionNewTopups != null && !oldShopOwnerIdOfTopupsCollectionNewTopups.equals(shopOwners)) {
                        oldShopOwnerIdOfTopupsCollectionNewTopups.getTopupsCollection().remove(topupsCollectionNewTopups);
                        oldShopOwnerIdOfTopupsCollectionNewTopups = em.merge(oldShopOwnerIdOfTopupsCollectionNewTopups);
                    }
                }
            }
            for (Purchases purchasesCollectionOldPurchases : purchasesCollectionOld) {
                if (!purchasesCollectionNew.contains(purchasesCollectionOldPurchases)) {
                    purchasesCollectionOldPurchases.setShopOwnerId(null);
                    purchasesCollectionOldPurchases = em.merge(purchasesCollectionOldPurchases);
                }
            }
            for (Purchases purchasesCollectionNewPurchases : purchasesCollectionNew) {
                if (!purchasesCollectionOld.contains(purchasesCollectionNewPurchases)) {
                    ShopOwners oldShopOwnerIdOfPurchasesCollectionNewPurchases = purchasesCollectionNewPurchases.getShopOwnerId();
                    purchasesCollectionNewPurchases.setShopOwnerId(shopOwners);
                    purchasesCollectionNewPurchases = em.merge(purchasesCollectionNewPurchases);
                    if (oldShopOwnerIdOfPurchasesCollectionNewPurchases != null && !oldShopOwnerIdOfPurchasesCollectionNewPurchases.equals(shopOwners)) {
                        oldShopOwnerIdOfPurchasesCollectionNewPurchases.getPurchasesCollection().remove(purchasesCollectionNewPurchases);
                        oldShopOwnerIdOfPurchasesCollectionNewPurchases = em.merge(oldShopOwnerIdOfPurchasesCollectionNewPurchases);
                    }
                }
            }
            for (BalanceDeposit balanceDepositCollectionOldBalanceDeposit : balanceDepositCollectionOld) {
                if (!balanceDepositCollectionNew.contains(balanceDepositCollectionOldBalanceDeposit)) {
                    balanceDepositCollectionOldBalanceDeposit.setShopOwnerId(null);
                    balanceDepositCollectionOldBalanceDeposit = em.merge(balanceDepositCollectionOldBalanceDeposit);
                }
            }
            for (BalanceDeposit balanceDepositCollectionNewBalanceDeposit : balanceDepositCollectionNew) {
                if (!balanceDepositCollectionOld.contains(balanceDepositCollectionNewBalanceDeposit)) {
                    ShopOwners oldShopOwnerIdOfBalanceDepositCollectionNewBalanceDeposit = balanceDepositCollectionNewBalanceDeposit.getShopOwnerId();
                    balanceDepositCollectionNewBalanceDeposit.setShopOwnerId(shopOwners);
                    balanceDepositCollectionNewBalanceDeposit = em.merge(balanceDepositCollectionNewBalanceDeposit);
                    if (oldShopOwnerIdOfBalanceDepositCollectionNewBalanceDeposit != null && !oldShopOwnerIdOfBalanceDepositCollectionNewBalanceDeposit.equals(shopOwners)) {
                        oldShopOwnerIdOfBalanceDepositCollectionNewBalanceDeposit.getBalanceDepositCollection().remove(balanceDepositCollectionNewBalanceDeposit);
                        oldShopOwnerIdOfBalanceDepositCollectionNewBalanceDeposit = em.merge(oldShopOwnerIdOfBalanceDepositCollectionNewBalanceDeposit);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = shopOwners.getShopOwnerId();
                if (findShopOwners(id) == null) {
                    throw new NonexistentEntityException("The shopOwners with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ShopOwners shopOwners;
            try {
                shopOwners = em.getReference(ShopOwners.class, id);
                shopOwners.getShopOwnerId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The shopOwners with id " + id + " no longer exists.", enfe);
            }
            Merchants merchantId = shopOwners.getMerchantId();
            if (merchantId != null) {
                merchantId.getShopOwnersCollection().remove(shopOwners);
                merchantId = em.merge(merchantId);
            }
            Collection<Topups> topupsCollection = shopOwners.getTopupsCollection();
            for (Topups topupsCollectionTopups : topupsCollection) {
                topupsCollectionTopups.setShopOwnerId(null);
                topupsCollectionTopups = em.merge(topupsCollectionTopups);
            }
            Collection<Purchases> purchasesCollection = shopOwners.getPurchasesCollection();
            for (Purchases purchasesCollectionPurchases : purchasesCollection) {
                purchasesCollectionPurchases.setShopOwnerId(null);
                purchasesCollectionPurchases = em.merge(purchasesCollectionPurchases);
            }
            Collection<BalanceDeposit> balanceDepositCollection = shopOwners.getBalanceDepositCollection();
            for (BalanceDeposit balanceDepositCollectionBalanceDeposit : balanceDepositCollection) {
                balanceDepositCollectionBalanceDeposit.setShopOwnerId(null);
                balanceDepositCollectionBalanceDeposit = em.merge(balanceDepositCollectionBalanceDeposit);
            }
            em.remove(shopOwners);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ShopOwners> findShopOwnersEntities() {
        return findShopOwnersEntities(true, -1, -1);
    }

    public List<ShopOwners> findShopOwnersEntities(int maxResults, int firstResult) {
        return findShopOwnersEntities(false, maxResults, firstResult);
    }

    private List<ShopOwners> findShopOwnersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ShopOwners.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ShopOwners findShopOwners(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ShopOwners.class, id);
        } finally {
            em.close();
        }
    }

    public int getShopOwnersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ShopOwners> rt = cq.from(ShopOwners.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<ShopOwners> findByIsActive(Character isActive, Integer limit) {
        return getEntityManager().createNamedQuery("ShopOwners.findByIsActive")
                .setParameter("isActive", isActive)
                .setMaxResults(limit).getResultList();
    }

    public ShopOwners findByEmail(String email) {
        try {
            return (ShopOwners) getEntityManager().createNamedQuery("ShopOwners.findByEmail")
                    .setParameter("email", email)
                    .getSingleResult();            
        } catch (NoResultException | NonUniqueResultException nre) {
            return null;
        }
    }    

    public ShopOwners findByWalletId(BigInteger walletId) {
        try {
            return (ShopOwners) getEntityManager().createNamedQuery("ShopOwners.findByWalletId")
                    .setParameter("walletId", walletId)
                    .getSingleResult();            
        } catch (NoResultException | NonUniqueResultException nre) {
            return null;
        } 
    }
    
}
