/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.session;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import alfamindtransactionmigration.entity.ShopOwners;
import alfamindtransactionmigration.entity.Topups;
import alfamindtransactionmigration.session.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

/**
 *
 * @author rudythong
 */
public class TopupsJpaController implements Serializable {

    public TopupsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Topups topups) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ShopOwners shopOwnerId = topups.getShopOwnerId();
            if (shopOwnerId != null) {
                shopOwnerId = em.getReference(shopOwnerId.getClass(), shopOwnerId.getShopOwnerId());
                topups.setShopOwnerId(shopOwnerId);
            }
            em.persist(topups);
            if (shopOwnerId != null) {
                shopOwnerId.getTopupsCollection().add(topups);
                shopOwnerId = em.merge(shopOwnerId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Topups topups) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Topups persistentTopups = em.find(Topups.class, topups.getTopupId());
            ShopOwners shopOwnerIdOld = persistentTopups.getShopOwnerId();
            ShopOwners shopOwnerIdNew = topups.getShopOwnerId();
            if (shopOwnerIdNew != null) {
                shopOwnerIdNew = em.getReference(shopOwnerIdNew.getClass(), shopOwnerIdNew.getShopOwnerId());
                topups.setShopOwnerId(shopOwnerIdNew);
            }
            topups = em.merge(topups);
            if (shopOwnerIdOld != null && !shopOwnerIdOld.equals(shopOwnerIdNew)) {
                shopOwnerIdOld.getTopupsCollection().remove(topups);
                shopOwnerIdOld = em.merge(shopOwnerIdOld);
            }
            if (shopOwnerIdNew != null && !shopOwnerIdNew.equals(shopOwnerIdOld)) {
                shopOwnerIdNew.getTopupsCollection().add(topups);
                shopOwnerIdNew = em.merge(shopOwnerIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = topups.getTopupId();
                if (findTopups(id) == null) {
                    throw new NonexistentEntityException("The topups with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Topups topups;
            try {
                topups = em.getReference(Topups.class, id);
                topups.getTopupId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The topups with id " + id + " no longer exists.", enfe);
            }
            ShopOwners shopOwnerId = topups.getShopOwnerId();
            if (shopOwnerId != null) {
                shopOwnerId.getTopupsCollection().remove(topups);
                shopOwnerId = em.merge(shopOwnerId);
            }
            em.remove(topups);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Topups> findTopupsEntities() {
        return findTopupsEntities(true, -1, -1);
    }

    public List<Topups> findTopupsEntities(int maxResults, int firstResult) {
        return findTopupsEntities(false, maxResults, firstResult);
    }

    private List<Topups> findTopupsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Topups.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Topups findTopups(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Topups.class, id);
        } finally {
            em.close();
        }
    }

    public int getTopupsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Topups> rt = cq.from(Topups.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public Topups findByInvoiceNumber(String invoiceNumber) {
        try {
            return (Topups) getEntityManager().createNamedQuery("Topups.findByInvoiceNumber").setParameter("invoiceNumber", invoiceNumber).getSingleResult();        
        } catch (NoResultException | NonUniqueResultException nre) {
            return null;
        }
    }

    public Topups findByShopOwnerId(ShopOwners shopOwner) {
        try {
            return (Topups) getEntityManager().createNamedQuery("Topups.findByShopOwner").setParameter("shopOwner", shopOwner).getSingleResult();        
        } catch (NoResultException | NonUniqueResultException nre) {
            return null;
        }
    }

    public boolean isUniqueInvoice(String invoiceNumber) {
        List<Topups> topups = getEntityManager().createNamedQuery("Topups.findByInvoiceNumber").setParameter("invoiceNumber", invoiceNumber).getResultList();
        return topups.isEmpty();
    }
}
