/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.session;

import alfamindtransactionmigration.entity.Wallet;
import alfamindtransactionmigration.session.exceptions.NonexistentEntityException;
import alfamindtransactionmigration.session.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.LockModeType;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author rudythong
 */
public class WalletJpaController implements Serializable {

    public WalletJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Wallet wallet) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(wallet);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findWallet(wallet.getWalletId()) != null) {
                throw new PreexistingEntityException("Wallet " + wallet + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Wallet wallet) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            wallet = em.merge(wallet);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = wallet.getWalletId();
                if (findWallet(id) == null) {
                    throw new NonexistentEntityException("The wallet with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Wallet wallet;
            try {
                wallet = em.getReference(Wallet.class, id);
                wallet.getWalletId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The wallet with id " + id + " no longer exists.", enfe);
            }
            em.remove(wallet);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Wallet> findWalletEntities() {
        return findWalletEntities(true, -1, -1);
    }

    public List<Wallet> findWalletEntities(int maxResults, int firstResult) {
        return findWalletEntities(false, maxResults, firstResult);
    }

    private List<Wallet> findWalletEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Wallet.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Wallet findWallet(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Wallet.class, id);
        } finally {
            em.close();
        }
    }

    public int getWalletCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Wallet> rt = cq.from(Wallet.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public Wallet updateWallet(Long walletId, BigDecimal amount, Date date) {
        Wallet wallet = getEntityManager().find(Wallet.class, walletId);
        if(wallet != null) {
            BigDecimal oldBalance  = wallet.getLastBalanceAfter();
            BigDecimal newBalance = oldBalance.add(amount);
            System.out.println("Wallet ID = " + walletId);
            System.out.println("Old Balance = " + oldBalance);
            System.out.println("New Balance = " + newBalance);
            if(amount.compareTo(BigDecimal.ZERO) == -1) {
                if(newBalance.compareTo(BigDecimal.ZERO) == -1) {
                    return null;
                } else {
                    wallet.setLastBalanceBefore(oldBalance);
                    wallet.setLastBalanceAfter(newBalance);
                    wallet.setTotalDebit(wallet.getTotalDebit().add(amount.negate()));                                    
                }
            } else {
                wallet.setLastBalanceBefore(oldBalance);
                wallet.setLastBalanceAfter(newBalance);
                wallet.setTotalCredit(wallet.getTotalCredit().add(amount));                
            }
            try {
                wallet.setUpdatedTimestamp(date);
                edit(wallet);
            } catch (Exception ex) {
                System.out.println("Never happen");
            }
        } else {
            System.out.println("WALLET NOT FOUND");
        }
        return wallet;
    }
}
