/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.session;

import alfamindtransactionmigration.entity.Migration;
import alfamindtransactionmigration.exceptions.NonexistentEntityException;
import alfamindtransactionmigration.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author rudythong
 */
public class MigrationJpaController implements Serializable {

    public MigrationJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Migration migration) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(migration);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMigration(migration.getNo()) != null) {
                throw new PreexistingEntityException("Migration " + migration + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Migration migration) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            migration = em.merge(migration);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = migration.getNo();
                if (findMigration(id) == null) {
                    throw new NonexistentEntityException("The migration with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Migration migration;
            try {
                migration = em.getReference(Migration.class, id);
                migration.getNo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The migration with id " + id + " no longer exists.", enfe);
            }
            em.remove(migration);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Migration> findMigrationEntities() {
        return findMigrationEntities(true, -1, -1);
    }

    public List<Migration> findMigrationEntities(int maxResults, int firstResult) {
        return findMigrationEntities(false, maxResults, firstResult);
    }

    private List<Migration> findMigrationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Migration.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Migration findMigration(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Migration.class, id);
        } finally {
            em.close();
        }
    }

    public int getMigrationCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Migration> rt = cq.from(Migration.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Migration> findByCardNo(String cardNo) {
        return getEntityManager().createNamedQuery("Migration.findByCardno").setParameter("cardno", cardNo).getResultList();        
    }

    public List<Migration> findEmailNotNull() {
        return getEntityManager().createQuery("SELECT m FROM Migration m WHERE m.email is not null ORDER BY m.transactionDate ASC, m.no ASC").getResultList();        
    }
}
