/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.session;

import alfamindtransactionmigration.entity.BalanceDeposit;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import alfamindtransactionmigration.entity.ShopOwners;
import alfamindtransactionmigration.session.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author rudythong
 */
public class BalanceDepositJpaController implements Serializable {

    public BalanceDepositJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(BalanceDeposit balanceDeposit) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ShopOwners shopOwnerId = balanceDeposit.getShopOwnerId();
            if (shopOwnerId != null) {
                shopOwnerId = em.getReference(shopOwnerId.getClass(), shopOwnerId.getShopOwnerId());
                balanceDeposit.setShopOwnerId(shopOwnerId);
            }
            em.persist(balanceDeposit);
            if (shopOwnerId != null) {
                shopOwnerId.getBalanceDepositCollection().add(balanceDeposit);
                shopOwnerId = em.merge(shopOwnerId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(BalanceDeposit balanceDeposit) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            BalanceDeposit persistentBalanceDeposit = em.find(BalanceDeposit.class, balanceDeposit.getBalanceDepositId());
            ShopOwners shopOwnerIdOld = persistentBalanceDeposit.getShopOwnerId();
            ShopOwners shopOwnerIdNew = balanceDeposit.getShopOwnerId();
            if (shopOwnerIdNew != null) {
                shopOwnerIdNew = em.getReference(shopOwnerIdNew.getClass(), shopOwnerIdNew.getShopOwnerId());
                balanceDeposit.setShopOwnerId(shopOwnerIdNew);
            }
            balanceDeposit = em.merge(balanceDeposit);
            if (shopOwnerIdOld != null && !shopOwnerIdOld.equals(shopOwnerIdNew)) {
                shopOwnerIdOld.getBalanceDepositCollection().remove(balanceDeposit);
                shopOwnerIdOld = em.merge(shopOwnerIdOld);
            }
            if (shopOwnerIdNew != null && !shopOwnerIdNew.equals(shopOwnerIdOld)) {
                shopOwnerIdNew.getBalanceDepositCollection().add(balanceDeposit);
                shopOwnerIdNew = em.merge(shopOwnerIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = balanceDeposit.getBalanceDepositId();
                if (findBalanceDeposit(id) == null) {
                    throw new NonexistentEntityException("The balanceDeposit with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            BalanceDeposit balanceDeposit;
            try {
                balanceDeposit = em.getReference(BalanceDeposit.class, id);
                balanceDeposit.getBalanceDepositId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The balanceDeposit with id " + id + " no longer exists.", enfe);
            }
            ShopOwners shopOwnerId = balanceDeposit.getShopOwnerId();
            if (shopOwnerId != null) {
                shopOwnerId.getBalanceDepositCollection().remove(balanceDeposit);
                shopOwnerId = em.merge(shopOwnerId);
            }
            em.remove(balanceDeposit);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<BalanceDeposit> findBalanceDepositEntities() {
        return findBalanceDepositEntities(true, -1, -1);
    }

    public List<BalanceDeposit> findBalanceDepositEntities(int maxResults, int firstResult) {
        return findBalanceDepositEntities(false, maxResults, firstResult);
    }

    private List<BalanceDeposit> findBalanceDepositEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(BalanceDeposit.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public BalanceDeposit findBalanceDeposit(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(BalanceDeposit.class, id);
        } finally {
            em.close();
        }
    }

    public int getBalanceDepositCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<BalanceDeposit> rt = cq.from(BalanceDeposit.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
