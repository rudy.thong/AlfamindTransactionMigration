/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration;

import alfamindtransactionmigration.entity.BalanceDeposit;
import alfamindtransactionmigration.entity.Migration;
import alfamindtransactionmigration.entity.Purchases;
import alfamindtransactionmigration.entity.ShopOwners;
import alfamindtransactionmigration.entity.Topups;
import alfamindtransactionmigration.entity.Wallet;
import alfamindtransactionmigration.session.BalanceDepositJpaController;
import alfamindtransactionmigration.session.MigrationJpaController;
import alfamindtransactionmigration.session.PurchasesJpaController;
import alfamindtransactionmigration.session.ShopOwnersJpaController;
import alfamindtransactionmigration.session.TopupsJpaController;
import alfamindtransactionmigration.session.WalletJpaController;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author rudythong
 */
public class AlfamindTransactionMigration {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        //get from table migration
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AlfamindTransactionMigrationPU");
        MigrationJpaController migrationJpaController = new MigrationJpaController(emf);
        ShopOwnersJpaController shopOwnersJpaController = new ShopOwnersJpaController(emf);
        WalletJpaController walletJpaController = new WalletJpaController(emf);
        PurchasesJpaController purchasesJpaController = new PurchasesJpaController(emf);
        TopupsJpaController topupsJpaController = new TopupsJpaController(emf);
        BalanceDepositJpaController balanceDepositJpaController = new BalanceDepositJpaController(emf);
        List<Migration> migrations = migrationJpaController.findEmailNotNull();
        for(Migration migration : migrations) {
            if(migration.getTransactionType().equalsIgnoreCase("Activation Conf") ||
                migration.getTransactionType().equalsIgnoreCase("Adjustment add") ||
                migration.getTransactionType().equalsIgnoreCase("Top up") || 
                migration.getTransactionType().equalsIgnoreCase("Top Up Conf")) {
                    ShopOwners shopOwners = shopOwnersJpaController.findByEmail(migration.getEmail());
                    BigDecimal amountDecimal = new BigDecimal(migration.getAmount());
                    Wallet wallet = walletJpaController.updateWallet(shopOwners.getWalletId().longValue(), amountDecimal, migration.getTransactionDate());
                    Topups topups = new Topups();
                    BalanceDeposit balanceDeposit = new BalanceDeposit();
                    topups.setTopupAmount(amountDecimal);
                    topups.setInvoiceNumber(migration.getInvoice());
                    topups.setTopupChannel("Alfamart");
                    balanceDeposit.setChannelType('P');
                    topups.setTopupStatus('S');
                    topups.setTopupTimestamp(migration.getTransactionDate());
                    topups.setRc("0000");
                    topups.setShopOwnerId(shopOwners);
                    balanceDeposit.setAfterAmount(wallet.getLastBalanceAfter());
                    balanceDeposit.setBeforeAmount(wallet.getLastBalanceBefore());
                    balanceDeposit.setAmount(amountDecimal);
                    balanceDeposit.setBalanceUpdatedTimestamp(migration.getTransactionDate());
                    balanceDeposit.setReference(migration.getInvoice());
                    balanceDeposit.setShopOwnerId(shopOwners);
                    balanceDepositJpaController.create(balanceDeposit);
                    topups.setBalanceCreditId(BigInteger.valueOf(balanceDeposit.getBalanceDepositId()));
                    topupsJpaController.create(topups);
            } else if(migration.getTransactionType().equalsIgnoreCase("Sales") || 
                migration.getTransactionType().equalsIgnoreCase("Adjustment Subst")) {
                    ShopOwners shopOwners = shopOwnersJpaController.findByEmail(migration.getEmail());
                    BigDecimal amountDecimal = new BigDecimal(migration.getAmount());
                    Wallet wallet = walletJpaController.updateWallet(shopOwners.getWalletId().longValue(), amountDecimal.negate(), migration.getTransactionDate());
                    if(wallet == null) {
                        System.out.println("Update Wallet failed for wallet id = " + shopOwners.getWalletId());
                    }
                    Purchases purchases = new Purchases();
                    BalanceDeposit balanceDeposit = new BalanceDeposit();
                    purchases.setPurchaseAmount(amountDecimal);
                    purchases.setInvoiceNumber(migration.getInvoice());
                    if(migration.getTransactionType().equalsIgnoreCase("Sales")) {
                        if(migration.getInvoice().startsWith("ADM")) {
                            purchases.setPurchaseChannel("Activation");
                            balanceDeposit.setChannelType('A');
                        } else {
                            purchases.setPurchaseChannel("Alfacart");                            
                            balanceDeposit.setChannelType('T');
                        }
                    } else if(migration.getTransactionType().equalsIgnoreCase("Adjustment Subst")) {                    
                        purchases.setPurchaseChannel("Manual");
                        balanceDeposit.setChannelType('T');
                    }
                    purchases.setPurchaseStatus('S');
                    purchases.setPurchaseTimestamp(migration.getTransactionDate());
                    purchases.setRc("0000");
                    purchases.setToken("");
                    purchases.setShopOwnerId(shopOwners);
                    balanceDeposit.setAfterAmount(wallet.getLastBalanceAfter());
                    balanceDeposit.setBeforeAmount(wallet.getLastBalanceBefore());
                    balanceDeposit.setAmount(amountDecimal.negate());
                    balanceDeposit.setBalanceUpdatedTimestamp(migration.getTransactionDate());
                    balanceDeposit.setReference(migration.getInvoice());
                    balanceDeposit.setShopOwnerId(shopOwners);
                    balanceDepositJpaController.create(balanceDeposit);
                    purchases.setBalanceDebitId(BigInteger.valueOf(balanceDeposit.getBalanceDepositId()));
                    purchasesJpaController.create(purchases);
            }
        }
    }
}
