/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration;

import alfamindtransactionmigration.session.MigrationJpaController;
import alfamindtransactionmigration.entity.Migration;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author rudythong
 */
public class FillInvoice {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AlfamindTransactionMigrationPU");
        MigrationJpaController transactionJpaController = new MigrationJpaController(emf);
        List<Migration> transactions = transactionJpaController.findMigrationEntities();
        for(Migration t : transactions) {
            if(t.getInvoice().isEmpty()) {
                BigDecimal random = new BigDecimal(String.valueOf(Math.random())).multiply(new BigDecimal("8999999999"));
                random = random.add(new BigDecimal("1000000000"));
                random = random.setScale(0, RoundingMode.CEILING);
                if(t.getTransactionType().equalsIgnoreCase("Top up")) {
                    t.setInvoice("MRG" + random.toPlainString());
                } else if(t.getTransactionType().equalsIgnoreCase("Adjustment add")) {
                    t.setInvoice("ADD" + random.toPlainString());
                } else if(t.getTransactionType().equalsIgnoreCase("Adjustment Subst")) {
                    t.setInvoice("SUB" + random.toPlainString());                    
                }
                try {
                    transactionJpaController.edit(t);
                    System.out.println("Edit no " + t.getNo() + " with invoice = " + t.getInvoice());
                } catch (Exception ex) {
                }
            }
        }
    }
}
