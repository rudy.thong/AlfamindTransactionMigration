/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rudythong
 */
@Entity
@Table(name = "balance_deposit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BalanceDeposit.findAll", query = "SELECT b FROM BalanceDeposit b"),
    @NamedQuery(name = "BalanceDeposit.findByBalanceDepositId", query = "SELECT b FROM BalanceDeposit b WHERE b.balanceDepositId = :balanceDepositId"),
    @NamedQuery(name = "BalanceDeposit.findByBeforeAmount", query = "SELECT b FROM BalanceDeposit b WHERE b.beforeAmount = :beforeAmount"),
    @NamedQuery(name = "BalanceDeposit.findByAfterAmount", query = "SELECT b FROM BalanceDeposit b WHERE b.afterAmount = :afterAmount"),
    @NamedQuery(name = "BalanceDeposit.findByBalanceUpdatedTimestamp", query = "SELECT b FROM BalanceDeposit b WHERE b.balanceUpdatedTimestamp = :balanceUpdatedTimestamp"),
    @NamedQuery(name = "BalanceDeposit.findByAmount", query = "SELECT b FROM BalanceDeposit b WHERE b.amount = :amount"),
    @NamedQuery(name = "BalanceDeposit.findByReference", query = "SELECT b FROM BalanceDeposit b WHERE b.reference = :reference"),
    @NamedQuery(name = "BalanceDeposit.findByChannelType", query = "SELECT b FROM BalanceDeposit b WHERE b.channelType = :channelType")})
public class BalanceDeposit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "balance_deposit_id")
    private Long balanceDepositId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "before_amount")
    private BigDecimal beforeAmount;
    @Column(name = "after_amount")
    private BigDecimal afterAmount;
    @Column(name = "balance_updated_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date balanceUpdatedTimestamp;
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "reference")
    private String reference;
    @Column(name = "channel_type")
    private Character channelType;
    @JoinColumn(name = "shop_owner_id", referencedColumnName = "shop_owner_id")
    @ManyToOne
    private ShopOwners shopOwnerId;

    public BalanceDeposit() {
    }

    public BalanceDeposit(Long balanceDepositId) {
        this.balanceDepositId = balanceDepositId;
    }

    public Long getBalanceDepositId() {
        return balanceDepositId;
    }

    public void setBalanceDepositId(Long balanceDepositId) {
        this.balanceDepositId = balanceDepositId;
    }

    public BigDecimal getBeforeAmount() {
        return beforeAmount;
    }

    public void setBeforeAmount(BigDecimal beforeAmount) {
        this.beforeAmount = beforeAmount;
    }

    public BigDecimal getAfterAmount() {
        return afterAmount;
    }

    public void setAfterAmount(BigDecimal afterAmount) {
        this.afterAmount = afterAmount;
    }

    public Date getBalanceUpdatedTimestamp() {
        return balanceUpdatedTimestamp;
    }

    public void setBalanceUpdatedTimestamp(Date balanceUpdatedTimestamp) {
        this.balanceUpdatedTimestamp = balanceUpdatedTimestamp;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Character getChannelType() {
        return channelType;
    }

    public void setChannelType(Character channelType) {
        this.channelType = channelType;
    }

    public ShopOwners getShopOwnerId() {
        return shopOwnerId;
    }

    public void setShopOwnerId(ShopOwners shopOwnerId) {
        this.shopOwnerId = shopOwnerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (balanceDepositId != null ? balanceDepositId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BalanceDeposit)) {
            return false;
        }
        BalanceDeposit other = (BalanceDeposit) object;
        if ((this.balanceDepositId == null && other.balanceDepositId != null) || (this.balanceDepositId != null && !this.balanceDepositId.equals(other.balanceDepositId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "alfamindtransactionmigration.BalanceDeposit[ balanceDepositId=" + balanceDepositId + " ]";
    }
    
}
