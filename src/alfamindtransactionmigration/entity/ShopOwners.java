/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author rudythong
 */
@Entity
@Table(name = "shop_owners")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ShopOwners.findAll", query = "SELECT s FROM ShopOwners s"),
    @NamedQuery(name = "ShopOwners.findByShopOwnerId", query = "SELECT s FROM ShopOwners s WHERE s.shopOwnerId = :shopOwnerId"),
    @NamedQuery(name = "ShopOwners.findByName", query = "SELECT s FROM ShopOwners s WHERE s.name = :name"),
    @NamedQuery(name = "ShopOwners.findByEmail", query = "SELECT s FROM ShopOwners s WHERE s.email = :email"),
    @NamedQuery(name = "ShopOwners.findByIsActive", query = "SELECT s FROM ShopOwners s WHERE s.isActive = :isActive"),
    @NamedQuery(name = "ShopOwners.findByPhone", query = "SELECT s FROM ShopOwners s WHERE s.phone = :phone"),
    @NamedQuery(name = "ShopOwners.findByDob", query = "SELECT s FROM ShopOwners s WHERE s.dob = :dob"),
    @NamedQuery(name = "ShopOwners.findByPob", query = "SELECT s FROM ShopOwners s WHERE s.pob = :pob"),
    @NamedQuery(name = "ShopOwners.findByIdentityType", query = "SELECT s FROM ShopOwners s WHERE s.identityType = :identityType"),
    @NamedQuery(name = "ShopOwners.findByIdentityNo", query = "SELECT s FROM ShopOwners s WHERE s.identityNo = :identityNo"),
    @NamedQuery(name = "ShopOwners.findByAddress", query = "SELECT s FROM ShopOwners s WHERE s.address = :address"),
    @NamedQuery(name = "ShopOwners.findByGender", query = "SELECT s FROM ShopOwners s WHERE s.gender = :gender"),
    @NamedQuery(name = "ShopOwners.findByCountry", query = "SELECT s FROM ShopOwners s WHERE s.country = :country"),
    @NamedQuery(name = "ShopOwners.findByJob", query = "SELECT s FROM ShopOwners s WHERE s.job = :job"),
    @NamedQuery(name = "ShopOwners.findByCity", query = "SELECT s FROM ShopOwners s WHERE s.city = :city"),
    @NamedQuery(name = "ShopOwners.findByZipcode", query = "SELECT s FROM ShopOwners s WHERE s.zipcode = :zipcode"),
    @NamedQuery(name = "ShopOwners.findByEducation", query = "SELECT s FROM ShopOwners s WHERE s.education = :education"),
    @NamedQuery(name = "ShopOwners.findByWalletId", query = "SELECT s FROM ShopOwners s WHERE s.walletId = :walletId"),
    @NamedQuery(name = "ShopOwners.findByCaptureId", query = "SELECT s FROM ShopOwners s WHERE s.captureId = :captureId")})
public class ShopOwners implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "shop_owner_id")
    private Integer shopOwnerId;
    @Column(name = "name")
    private String name;
    @Column(name = "email")
    private String email;
    @Column(name = "is_active")
    private Character isActive;
    @Column(name = "phone")
    private String phone;
    @Column(name = "dob")
    @Temporal(TemporalType.DATE)
    private Date dob;
    @Column(name = "pob")
    private String pob;
    @Column(name = "identity_type")
    private String identityType;
    @Column(name = "identity_no")
    private String identityNo;
    @Column(name = "address")
    private String address;
    @Column(name = "gender")
    private Character gender;
    @Column(name = "country")
    private String country;
    @Column(name = "job")
    private String job;
    @Column(name = "city")
    private String city;
    @Column(name = "zipcode")
    private String zipcode;
    @Column(name = "education")
    private String education;
    @Column(name = "wallet_id")
    private BigInteger walletId;
    @Column(name = "capture_id")
    private String captureId;
    @OneToMany(mappedBy = "shopOwnerId")
    private Collection<Topups> topupsCollection;
    @OneToMany(mappedBy = "shopOwnerId")
    private Collection<Purchases> purchasesCollection;
    @JoinColumn(name = "merchant_id", referencedColumnName = "merchant_id")
    @ManyToOne
    private Merchants merchantId;
    @OneToMany(mappedBy = "shopOwnerId")
    private Collection<BalanceDeposit> balanceDepositCollection;

    public ShopOwners() {
    }

    public ShopOwners(Integer shopOwnerId) {
        this.shopOwnerId = shopOwnerId;
    }

    public Integer getShopOwnerId() {
        return shopOwnerId;
    }

    public void setShopOwnerId(Integer shopOwnerId) {
        this.shopOwnerId = shopOwnerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getPob() {
        return pob;
    }

    public void setPob(String pob) {
        this.pob = pob;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public BigInteger getWalletId() {
        return walletId;
    }

    public void setWalletId(BigInteger walletId) {
        this.walletId = walletId;
    }

    public String getCaptureId() {
        return captureId;
    }

    public void setCaptureId(String captureId) {
        this.captureId = captureId;
    }

    @XmlTransient
    public Collection<Topups> getTopupsCollection() {
        return topupsCollection;
    }

    public void setTopupsCollection(Collection<Topups> topupsCollection) {
        this.topupsCollection = topupsCollection;
    }

    @XmlTransient
    public Collection<Purchases> getPurchasesCollection() {
        return purchasesCollection;
    }

    public void setPurchasesCollection(Collection<Purchases> purchasesCollection) {
        this.purchasesCollection = purchasesCollection;
    }

    public Merchants getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Merchants merchantId) {
        this.merchantId = merchantId;
    }

    @XmlTransient
    public Collection<BalanceDeposit> getBalanceDepositCollection() {
        return balanceDepositCollection;
    }

    public void setBalanceDepositCollection(Collection<BalanceDeposit> balanceDepositCollection) {
        this.balanceDepositCollection = balanceDepositCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shopOwnerId != null ? shopOwnerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ShopOwners)) {
            return false;
        }
        ShopOwners other = (ShopOwners) object;
        if ((this.shopOwnerId == null && other.shopOwnerId != null) || (this.shopOwnerId != null && !this.shopOwnerId.equals(other.shopOwnerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "alfamindtransactionmigration.ShopOwners[ shopOwnerId=" + shopOwnerId + " ]";
    }
    
}
