/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rudythong
 */
@Entity
@Table(name = "topups")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Topups.findAll", query = "SELECT t FROM Topups t"),
    @NamedQuery(name = "Topups.findByTopupId", query = "SELECT t FROM Topups t WHERE t.topupId = :topupId"),
    @NamedQuery(name = "Topups.findByTopupStatus", query = "SELECT t FROM Topups t WHERE t.topupStatus = :topupStatus"),
    @NamedQuery(name = "Topups.findByTopupTimestamp", query = "SELECT t FROM Topups t WHERE t.topupTimestamp = :topupTimestamp"),
    @NamedQuery(name = "Topups.findByTopupAmount", query = "SELECT t FROM Topups t WHERE t.topupAmount = :topupAmount"),
    @NamedQuery(name = "Topups.findByInvoiceNumber", query = "SELECT t FROM Topups t WHERE t.invoiceNumber = :invoiceNumber"),
    @NamedQuery(name = "Topups.findByBasket", query = "SELECT t FROM Topups t WHERE t.basket = :basket"),
    @NamedQuery(name = "Topups.findByBalanceCreditId", query = "SELECT t FROM Topups t WHERE t.balanceCreditId = :balanceCreditId"),
    @NamedQuery(name = "Topups.findByBalanceDebitId", query = "SELECT t FROM Topups t WHERE t.balanceDebitId = :balanceDebitId"),
    @NamedQuery(name = "Topups.findByTopupChannel", query = "SELECT t FROM Topups t WHERE t.topupChannel = :topupChannel"),
    @NamedQuery(name = "Topups.findByRc", query = "SELECT t FROM Topups t WHERE t.rc = :rc")})
public class Topups implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "topup_id")
    private Long topupId;
    @Column(name = "topup_status")
    private Character topupStatus;
    @Column(name = "topup_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date topupTimestamp;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "topup_amount")
    private BigDecimal topupAmount;
    @Column(name = "invoice_number")
    private String invoiceNumber;
    @Column(name = "basket")
    private String basket;
    @Column(name = "balance_credit_id")
    private BigInteger balanceCreditId;
    @Column(name = "balance_debit_id")
    private BigInteger balanceDebitId;
    @Column(name = "topup_channel")
    private String topupChannel;
    @Column(name = "rc")
    private String rc;
    @JoinColumn(name = "shop_owner_id", referencedColumnName = "shop_owner_id")
    @ManyToOne
    private ShopOwners shopOwnerId;

    public Topups() {
    }

    public Topups(Long topupId) {
        this.topupId = topupId;
    }

    public Long getTopupId() {
        return topupId;
    }

    public void setTopupId(Long topupId) {
        this.topupId = topupId;
    }

    public Character getTopupStatus() {
        return topupStatus;
    }

    public void setTopupStatus(Character topupStatus) {
        this.topupStatus = topupStatus;
    }

    public Date getTopupTimestamp() {
        return topupTimestamp;
    }

    public void setTopupTimestamp(Date topupTimestamp) {
        this.topupTimestamp = topupTimestamp;
    }

    public BigDecimal getTopupAmount() {
        return topupAmount;
    }

    public void setTopupAmount(BigDecimal topupAmount) {
        this.topupAmount = topupAmount;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getBasket() {
        return basket;
    }

    public void setBasket(String basket) {
        this.basket = basket;
    }

    public BigInteger getBalanceCreditId() {
        return balanceCreditId;
    }

    public void setBalanceCreditId(BigInteger balanceCreditId) {
        this.balanceCreditId = balanceCreditId;
    }

    public BigInteger getBalanceDebitId() {
        return balanceDebitId;
    }

    public void setBalanceDebitId(BigInteger balanceDebitId) {
        this.balanceDebitId = balanceDebitId;
    }

    public String getTopupChannel() {
        return topupChannel;
    }

    public void setTopupChannel(String topupChannel) {
        this.topupChannel = topupChannel;
    }

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public ShopOwners getShopOwnerId() {
        return shopOwnerId;
    }

    public void setShopOwnerId(ShopOwners shopOwnerId) {
        this.shopOwnerId = shopOwnerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (topupId != null ? topupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Topups)) {
            return false;
        }
        Topups other = (Topups) object;
        if ((this.topupId == null && other.topupId != null) || (this.topupId != null && !this.topupId.equals(other.topupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "alfamindtransactionmigration.Topups[ topupId=" + topupId + " ]";
    }
    
}
