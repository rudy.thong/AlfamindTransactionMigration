/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rudythong
 */
@Entity
@Table(name = "purchases")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Purchases.findAll", query = "SELECT p FROM Purchases p"),
    @NamedQuery(name = "Purchases.findByPurchaseId", query = "SELECT p FROM Purchases p WHERE p.purchaseId = :purchaseId"),
    @NamedQuery(name = "Purchases.findByPurchaseStatus", query = "SELECT p FROM Purchases p WHERE p.purchaseStatus = :purchaseStatus"),
    @NamedQuery(name = "Purchases.findByPurchaseTimestamp", query = "SELECT p FROM Purchases p WHERE p.purchaseTimestamp = :purchaseTimestamp"),
    @NamedQuery(name = "Purchases.findByPurchaseAmount", query = "SELECT p FROM Purchases p WHERE p.purchaseAmount = :purchaseAmount"),
    @NamedQuery(name = "Purchases.findByInvoiceNumber", query = "SELECT p FROM Purchases p WHERE p.invoiceNumber = :invoiceNumber"),
    @NamedQuery(name = "Purchases.findByBasket", query = "SELECT p FROM Purchases p WHERE p.basket = :basket"),
    @NamedQuery(name = "Purchases.findByBalanceDebitId", query = "SELECT p FROM Purchases p WHERE p.balanceDebitId = :balanceDebitId"),
    @NamedQuery(name = "Purchases.findByBalanceCreditId", query = "SELECT p FROM Purchases p WHERE p.balanceCreditId = :balanceCreditId"),
    @NamedQuery(name = "Purchases.findByToken", query = "SELECT p FROM Purchases p WHERE p.token = :token"),
    @NamedQuery(name = "Purchases.findByPurchaseChannel", query = "SELECT p FROM Purchases p WHERE p.purchaseChannel = :purchaseChannel"),
    @NamedQuery(name = "Purchases.findByRc", query = "SELECT p FROM Purchases p WHERE p.rc = :rc")})
public class Purchases implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "purchase_id")
    private Long purchaseId;
    @Column(name = "purchase_status")
    private Character purchaseStatus;
    @Column(name = "purchase_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date purchaseTimestamp;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "purchase_amount")
    private BigDecimal purchaseAmount;
    @Column(name = "invoice_number")
    private String invoiceNumber;
    @Column(name = "basket")
    private String basket;
    @Column(name = "balance_debit_id")
    private BigInteger balanceDebitId;
    @Column(name = "balance_credit_id")
    private BigInteger balanceCreditId;
    @Column(name = "token")
    private String token;
    @Column(name = "purchase_channel")
    private String purchaseChannel;
    @Column(name = "rc")
    private String rc;
    @JoinColumn(name = "shop_owner_id", referencedColumnName = "shop_owner_id")
    @ManyToOne
    private ShopOwners shopOwnerId;

    public Purchases() {
    }

    public Purchases(Long purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Long getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Long purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Character getPurchaseStatus() {
        return purchaseStatus;
    }

    public void setPurchaseStatus(Character purchaseStatus) {
        this.purchaseStatus = purchaseStatus;
    }

    public Date getPurchaseTimestamp() {
        return purchaseTimestamp;
    }

    public void setPurchaseTimestamp(Date purchaseTimestamp) {
        this.purchaseTimestamp = purchaseTimestamp;
    }

    public BigDecimal getPurchaseAmount() {
        return purchaseAmount;
    }

    public void setPurchaseAmount(BigDecimal purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getBasket() {
        return basket;
    }

    public void setBasket(String basket) {
        this.basket = basket;
    }

    public BigInteger getBalanceDebitId() {
        return balanceDebitId;
    }

    public void setBalanceDebitId(BigInteger balanceDebitId) {
        this.balanceDebitId = balanceDebitId;
    }

    public BigInteger getBalanceCreditId() {
        return balanceCreditId;
    }

    public void setBalanceCreditId(BigInteger balanceCreditId) {
        this.balanceCreditId = balanceCreditId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPurchaseChannel() {
        return purchaseChannel;
    }

    public void setPurchaseChannel(String purchaseChannel) {
        this.purchaseChannel = purchaseChannel;
    }

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public ShopOwners getShopOwnerId() {
        return shopOwnerId;
    }

    public void setShopOwnerId(ShopOwners shopOwnerId) {
        this.shopOwnerId = shopOwnerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (purchaseId != null ? purchaseId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Purchases)) {
            return false;
        }
        Purchases other = (Purchases) object;
        if ((this.purchaseId == null && other.purchaseId != null) || (this.purchaseId != null && !this.purchaseId.equals(other.purchaseId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "alfamindtransactionmigration.Purchases[ purchaseId=" + purchaseId + " ]";
    }
    
}
