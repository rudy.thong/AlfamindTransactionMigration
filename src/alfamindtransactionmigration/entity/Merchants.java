/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author rudythong
 */
@Entity
@Table(name = "merchants")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Merchants.findAll", query = "SELECT m FROM Merchants m"),
    @NamedQuery(name = "Merchants.findByMerchantId", query = "SELECT m FROM Merchants m WHERE m.merchantId = :merchantId"),
    @NamedQuery(name = "Merchants.findByCredentials", query = "SELECT m FROM Merchants m WHERE m.credentials = :credentials"),
    @NamedQuery(name = "Merchants.findByUrl", query = "SELECT m FROM Merchants m WHERE m.url = :url"),
    @NamedQuery(name = "Merchants.findByIp", query = "SELECT m FROM Merchants m WHERE m.ip = :ip"),
    @NamedQuery(name = "Merchants.findByTotalRegister", query = "SELECT m FROM Merchants m WHERE m.totalRegister = :totalRegister"),
    @NamedQuery(name = "Merchants.findByTotalApiCall", query = "SELECT m FROM Merchants m WHERE m.totalApiCall = :totalApiCall"),
    @NamedQuery(name = "Merchants.findBySessionKey", query = "SELECT m FROM Merchants m WHERE m.sessionKey = :sessionKey"),
    @NamedQuery(name = "Merchants.findByMallid", query = "SELECT m FROM Merchants m WHERE m.mallid = :mallid"),
    @NamedQuery(name = "Merchants.findByChainmerchant", query = "SELECT m FROM Merchants m WHERE m.chainmerchant = :chainmerchant"),
    @NamedQuery(name = "Merchants.findBySharedKey", query = "SELECT m FROM Merchants m WHERE m.sharedKey = :sharedKey")})
public class Merchants implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "merchant_id")
    private Integer merchantId;
    @Column(name = "credentials")
    private String credentials;
    @Column(name = "url")
    private String url;
    @Column(name = "ip")
    private String ip;
    @Column(name = "total_register")
    private Integer totalRegister;
    @Column(name = "total_api_call")
    private BigInteger totalApiCall;
    @Column(name = "session_key")
    private String sessionKey;
    @Column(name = "mallid")
    private Integer mallid;
    @Column(name = "chainmerchant")
    private Integer chainmerchant;
    @Column(name = "shared_key")
    private String sharedKey;
    @OneToMany(mappedBy = "merchantId")
    private Collection<ShopOwners> shopOwnersCollection;

    public Merchants() {
    }

    public Merchants(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getTotalRegister() {
        return totalRegister;
    }

    public void setTotalRegister(Integer totalRegister) {
        this.totalRegister = totalRegister;
    }

    public BigInteger getTotalApiCall() {
        return totalApiCall;
    }

    public void setTotalApiCall(BigInteger totalApiCall) {
        this.totalApiCall = totalApiCall;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public Integer getMallid() {
        return mallid;
    }

    public void setMallid(Integer mallid) {
        this.mallid = mallid;
    }

    public Integer getChainmerchant() {
        return chainmerchant;
    }

    public void setChainmerchant(Integer chainmerchant) {
        this.chainmerchant = chainmerchant;
    }

    public String getSharedKey() {
        return sharedKey;
    }

    public void setSharedKey(String sharedKey) {
        this.sharedKey = sharedKey;
    }

    @XmlTransient
    public Collection<ShopOwners> getShopOwnersCollection() {
        return shopOwnersCollection;
    }

    public void setShopOwnersCollection(Collection<ShopOwners> shopOwnersCollection) {
        this.shopOwnersCollection = shopOwnersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (merchantId != null ? merchantId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Merchants)) {
            return false;
        }
        Merchants other = (Merchants) object;
        if ((this.merchantId == null && other.merchantId != null) || (this.merchantId != null && !this.merchantId.equals(other.merchantId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "alfamindtransactionmigration.Merchants[ merchantId=" + merchantId + " ]";
    }
    
}
