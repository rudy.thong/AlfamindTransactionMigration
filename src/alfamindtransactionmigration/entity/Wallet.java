/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rudythong
 */
@Entity
@Table(name = "wallet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Wallet.findAll", query = "SELECT w FROM Wallet w"),
    @NamedQuery(name = "Wallet.findByWalletId", query = "SELECT w FROM Wallet w WHERE w.walletId = :walletId"),
    @NamedQuery(name = "Wallet.findByLastBalanceAfter", query = "SELECT w FROM Wallet w WHERE w.lastBalanceAfter = :lastBalanceAfter"),
    @NamedQuery(name = "Wallet.findByTotalDebit", query = "SELECT w FROM Wallet w WHERE w.totalDebit = :totalDebit"),
    @NamedQuery(name = "Wallet.findByTotalCredit", query = "SELECT w FROM Wallet w WHERE w.totalCredit = :totalCredit"),
    @NamedQuery(name = "Wallet.findByLastBalanceBefore", query = "SELECT w FROM Wallet w WHERE w.lastBalanceBefore = :lastBalanceBefore"),
    @NamedQuery(name = "Wallet.findByEncPin", query = "SELECT w FROM Wallet w WHERE w.encPin = :encPin"),
    @NamedQuery(name = "Wallet.findByUpdatedTimestamp", query = "SELECT w FROM Wallet w WHERE w.updatedTimestamp = :updatedTimestamp")})
public class Wallet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "wallet_id")
    private Long walletId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "last_balance_after")
    private BigDecimal lastBalanceAfter;
    @Column(name = "total_debit")
    private BigDecimal totalDebit;
    @Column(name = "total_credit")
    private BigDecimal totalCredit;
    @Column(name = "last_balance_before")
    private BigDecimal lastBalanceBefore;
    @Column(name = "enc_pin")
    private String encPin;
    @Column(name = "updated_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTimestamp;

    public Wallet() {
    }

    public Wallet(Long walletId) {
        this.walletId = walletId;
    }

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public BigDecimal getLastBalanceAfter() {
        return lastBalanceAfter;
    }

    public void setLastBalanceAfter(BigDecimal lastBalanceAfter) {
        this.lastBalanceAfter = lastBalanceAfter;
    }

    public BigDecimal getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(BigDecimal totalDebit) {
        this.totalDebit = totalDebit;
    }

    public BigDecimal getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(BigDecimal totalCredit) {
        this.totalCredit = totalCredit;
    }

    public BigDecimal getLastBalanceBefore() {
        return lastBalanceBefore;
    }

    public void setLastBalanceBefore(BigDecimal lastBalanceBefore) {
        this.lastBalanceBefore = lastBalanceBefore;
    }

    public String getEncPin() {
        return encPin;
    }

    public void setEncPin(String encPin) {
        this.encPin = encPin;
    }

    public Date getUpdatedTimestamp() {
        return updatedTimestamp;
    }

    public void setUpdatedTimestamp(Date updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (walletId != null ? walletId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Wallet)) {
            return false;
        }
        Wallet other = (Wallet) object;
        if ((this.walletId == null && other.walletId != null) || (this.walletId != null && !this.walletId.equals(other.walletId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "alfamindtransactionmigration.Wallet[ walletId=" + walletId + " ]";
    }
    
}
