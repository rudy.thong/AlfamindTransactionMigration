/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rudythong
 */
@Entity
@Table(name = "migration")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Migration.findAll", query = "SELECT m FROM Migration m"),
    @NamedQuery(name = "Migration.findByNo", query = "SELECT m FROM Migration m WHERE m.no = :no"),
    @NamedQuery(name = "Migration.findByCardno", query = "SELECT m FROM Migration m WHERE m.cardno = :cardno"),
    @NamedQuery(name = "Migration.findByFullname", query = "SELECT m FROM Migration m WHERE m.fullname = :fullname"),
    @NamedQuery(name = "Migration.findByTransactionType", query = "SELECT m FROM Migration m WHERE m.transactionType = :transactionType"),
    @NamedQuery(name = "Migration.findByTransactionDate", query = "SELECT m FROM Migration m WHERE m.transactionDate = :transactionDate"),
    @NamedQuery(name = "Migration.findByAmount", query = "SELECT m FROM Migration m WHERE m.amount = :amount"),
    @NamedQuery(name = "Migration.findByInvoice", query = "SELECT m FROM Migration m WHERE m.invoice = :invoice"),
    @NamedQuery(name = "Migration.findByIdCustomerKartuku", query = "SELECT m FROM Migration m WHERE m.idCustomerKartuku = :idCustomerKartuku"),
    @NamedQuery(name = "Migration.findByEmail", query = "SELECT m FROM Migration m WHERE m.email = :email"),
    @NamedQuery(name = "Migration.findByNameCustomerKartuku", query = "SELECT m FROM Migration m WHERE m.nameCustomerKartuku = :nameCustomerKartuku")})
public class Migration implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "no")
    private Integer no;
    @Column(name = "cardno")
    private String cardno;
    @Column(name = "fullname")
    private String fullname;
    @Column(name = "transaction_type")
    private String transactionType;
    @Column(name = "transaction_date")
    @Temporal(TemporalType.DATE)
    private Date transactionDate;
    @Column(name = "amount")
    private BigInteger amount;
    @Column(name = "invoice")
    private String invoice;
    @Column(name = "id_customer_kartuku")
    private String idCustomerKartuku;
    @Column(name = "email")
    private String email;
    @Column(name = "name_customer_kartuku")
    private String nameCustomerKartuku;

    public Migration() {
    }

    public Migration(Integer no) {
        this.no = no;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getIdCustomerKartuku() {
        return idCustomerKartuku;
    }

    public void setIdCustomerKartuku(String idCustomerKartuku) {
        this.idCustomerKartuku = idCustomerKartuku;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNameCustomerKartuku() {
        return nameCustomerKartuku;
    }

    public void setNameCustomerKartuku(String nameCustomerKartuku) {
        this.nameCustomerKartuku = nameCustomerKartuku;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (no != null ? no.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Migration)) {
            return false;
        }
        Migration other = (Migration) object;
        if ((this.no == null && other.no != null) || (this.no != null && !this.no.equals(other.no))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "alfamindtransactionmigration.Migration[ no=" + no + " ]";
    }
    
}
