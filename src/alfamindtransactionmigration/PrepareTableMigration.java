/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alfamindtransactionmigration;

import alfamindtransactionmigration.session.MigrationJpaController;
import alfamindtransactionmigration.entity.Migration;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author rudythong
 */
public class PrepareTableMigration {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //load JSON file
        String jsonString = "";
        FileReader fr = null;
        try {
            fr = new FileReader("ShopOwnersFull.json");
        } catch (FileNotFoundException ex) {
            System.out.println("Pasti dapat file nya!");
        }
        String line;
        BufferedReader br = new BufferedReader(fr);
        try {
            while ((line = br.readLine()) != null) {
                jsonString += line;
            }
        } catch (IOException ex) {
            System.out.println("Seharusnya tidak ada error ini");
        }
        //System.out.println("JSON = " + jsonString);

        //parse JSON
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AlfamindTransactionMigrationPU");
        JSONObject json = new JSONObject(jsonString);
        System.out.println("Status = " + json.getString("status"));
        System.out.println("Message = " + json.getString("message"));
        System.out.println("Total Data = " + json.getJSONObject("data").getInt("total"));
        List<String> checkUniqueEmail = new LinkedList<>();
        JSONArray jsonArray = json.getJSONObject("data").getJSONArray("so_list");
        for(int i = 0 ; i < jsonArray.length() ; i++) {
            JSONObject jsonData = jsonArray.getJSONObject(i);
            String email = jsonData.getString("email");
            if(checkUniqueEmail.contains(email)) {
                System.out.println("Email ini duplikat = " + email);
            } else {
                checkUniqueEmail.add(email);
            }
            String name = jsonData.getString("customerName");
            String cardNo = jsonData.getString("id_customer_kartuku");
            System.out.println("email = " + email + " name = " + name + " cardNo = " + cardNo);
            MigrationJpaController transactionJpaController = new MigrationJpaController(emf);
            List<Migration> transactions = transactionJpaController.findByCardNo(cardNo);
            for(Migration t : transactions) {
                t.setIdCustomerKartuku(cardNo);
                t.setEmail(email);
                t.setNameCustomerKartuku(name);
                try {
                    transactionJpaController.edit(t);
                } catch (Exception ex) {
                    System.out.println("Gak mungkin!");
                }
                System.out.println("Success edit transation with id = " + t.getNo().toString());
            }
        }
    }
}
